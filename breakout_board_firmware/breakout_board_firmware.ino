int commandByte = 0;
int checksumByte = 0;
int currentByte = 0;

#define RED 1
#define BLUE 2
#define GREEN 3
#define OFF 4

// This function is called when the program starts.
void setup() {
  pinMode(9,OUTPUT); // This will be our voltage for the heat switch relay
  digitalWrite(9,LOW);  //maybe this will keep the 1 pin low to start with
  pinMode(2,OUTPUT);
  pinMode(3,OUTPUT);
  pinMode(4,OUTPUT);
  pinMode(5,OUTPUT);
  pinMode(6,OUTPUT);
  pinMode(7,OUTPUT);
 
  set_color(GREEN);
 
  Serial.begin(9600);
}

// set_relay_state(int): change the state of the relay. -- same for heat switch relay
//   state = 1 -> relay closed.
//   state = 0 -> relay open.
void set_relay_state(int state) {
  if(state == 1) digitalWrite(2, HIGH);
  else if(state == 0) digitalWrite(2, LOW);
}
void set_switch_relay_state(int state) { // this flips the power to the heat switch
  if(state ==1) {
    digitalWrite(9, HIGH); // trigger the relay for the heat switch power
    delay(2000);
  }
  else if(state ==0) digitalWrite(9, LOW); // kill the connection for the heat switch power
 
}
// This function validates that the command and checksum bytes are valid.
int validate_command() {
  if( 127 - checksumByte == commandByte ) return 1;
  else return 0;
}

void set_color(int color) {
  // Turn off the whole thing
   
  digitalWrite(5,LOW);
  digitalWrite(6,LOW);
  digitalWrite(7,LOW);
  if(color == RED) digitalWrite(5,HIGH); 
  else if(color == GREEN) digitalWrite(6,HIGH);
  else if(color == BLUE) digitalWrite(7,HIGH);
}

// This function executes a command.
int execute_command() {
  switch(commandByte) {
    case 0x4D: // M, for magnetization cycle
      set_relay_state(1); // Close the relay, activating high power mode.
      Serial.print("OK: Magnetization cycle.\n");
      set_color(RED);
      break;
    case 0x52: // R, for regulation cycle
      set_relay_state(0); // Open the relay, activating high dynamic range mode.
      Serial.print("OK: Regulation cycle.\n");
      set_color(BLUE);
      break;
    case 0x4F: 
      set_switch_relay_state(1); // power the relay, applying power to the switch.	
      digitalWrite(4, HIGH);
      delay(100);
      digitalWrite(4, LOW);
      delay(10000);
      Serial.print("Initiated heat switch: OPEN command\n");
      set_switch_relay_state(0); // turn off power to the relay, cutting the power to the switch.
      break;
   case 0x43:
      set_switch_relay_state(1); // power the relay, applying power to the switch.
      digitalWrite(4, HIGH);//I think we need to flex the open switch first before the close will work
      delay(100);
      digitalWrite(4, LOW);
      delay(10000);
      digitalWrite(3, HIGH);
      delay(100);
      digitalWrite(3, LOW);
      delay(10000);
      Serial.print("Initiated heat switch: CLOSE command\n");
      set_switch_relay_state(0); // turn off power to the relay, cutting the power to the switch.
      break;
   default:
      Serial.print("Unrecognized command.\n");
    
  }
}

// This function is called periodically forever.
void loop() {
  //set_switch_relay_state(0);// keep the switch realy off most of the time
  digitalWrite(1,LOW);//will this keep pin 1 low almost always?
  if(Serial.available() > 0) {
    // Read in the new byte.
    currentByte = Serial.read();
    // Check if the current byte is a newline (the "execute" command).
    if(currentByte == 0x0A) {
      if(validate_command()) {
        execute_command();
      } else {
        Serial.print("Invalid command checksum.\n");
      }
    } else {
      // No command is being executed. Spill the bytes over.
      commandByte   = checksumByte;
      checksumByte  = currentByte;
      currentByte = 0;
    }
  }
}
