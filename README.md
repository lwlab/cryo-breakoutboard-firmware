# cryo-breakoutboard-firmware #

This software is written for an Arduino UNO, which lives inside of the breakout board (BOB) for the cryostat. The purpose of this Arduino is to provide electronic access to several previously manual operations:

 - Operating the heat switch
 - Switching between magnet cycle and regulation

## Control and communication

In order to control this Arduino, plug a male-male USB cable into the back of the BOB. The Arduino operates as a serial port, on linux appearing as something like "/dev/ttyACM0". 

The communication protocol is simple, and requires only three characters. 

1. Command character
2. Checksum character
3. Newline ('\n')

The command characters are as follows:

 - Switch to magnetization cycle: "M"
 - Switch to regulation cycle: "R"
 - Close heat switch: "C"
 - Open heat switch: "O"

The checksum is generated as follows:

	checksum_byte = 127 - command_byte

So, for example, the checksum byte for magnetization cycle (command byte = "M" = 0x4D) is "2" = 0x32. So the full command to begin magnetization is, in hex form:

	[ 0x4D, 0x32, 0x0A ]

or in ASCII form:

	M2\n

(where \n is the newline character).

## Serial connection settings ##

Use 9600 baud, with no line endings and no stop bits.